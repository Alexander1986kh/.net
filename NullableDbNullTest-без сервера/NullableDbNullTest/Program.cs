﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullableDbNullTest
{
    class Program
    {
        static void Main(string[] args)
        {
         
            int? x = 10;// Тип данных становится ссылочным, он может хранить пустую ссылку
            int? y = 10;
            /*int? n = null;
            //int m1 = n;//Даже не откомпилируется
            // Неявное приведение типов не работает
            int m2 = (int)n;// явное приведение типов работает, 
            //Получим исключение во время выполнения System.InvalidOperationException
            // во время выполнения программы
            int m3 = n.Value;*/
            if (x.HasValue)// if (y!=null)
            {
                System.Console.WriteLine(x.Value);
            }
            else
            {
                System.Console.WriteLine("Undefined");
            }
            #region Сравнение типов
            int? num1 = 10;
            int? num2 = null;
            if (num1 == num2)
                Console.WriteLine("num1 is greater than num2");
            else
            //выполнится эта ветка, но значение в num1 не меньше значения num2
            {
                Console.WriteLine("num1>=num2 returned false(but num1<num2 also is false)");
            }
            if (num1 != num2)
                Console.WriteLine("Finaly, num1!=num2, return true");
            num1 = null;
            if (num1 == num2)
            {
                Console.WriteLine("Два значения неразличимы- равны друг другу");
            }
            int? c = null;
            // d=c, если значение с null,
            // то в переменную значимого типа d=-1;
            int d = c ?? -1;
            //упаковка в ссылочный тип
            bool? b = null;
            object o=b;//Now is null
            b = false;
            int? i = 44;
            object bBoxed = b;//bBoxed contains  a b
            object iBoxed = i;//iBoxed contains a i
            int X = 10;// int- это 4 байта
            object xBoxed = X;// object это 10 байт
            ArrayList arr = new ArrayList();
            arr.Add(x);
            #endregion
     


        }
    }
    public static class MyExtensions
    {
       
    }
}
