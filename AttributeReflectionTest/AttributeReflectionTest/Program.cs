﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AttributeReflectionTest
{
    class Program
    {
        /*
        static void Main(string[] args)
        {
            Console.WriteLine("Привет, как Вас зовут?");
            string userName = Console.ReadLine();
            Console.WriteLine("Привет " + userName + ", нажмите любую клавишу...");
            Console.ReadKey();
        }
        */
        #region Тест рефлексии: исследование "чужого кода"
        /*
    static void Main(string[] args)
    {
        try
        {
            Assembly asm = Assembly.LoadFrom("MyAssembly.exe");
            Type t = asm.GetType("MyAssembly.Program", true, true);
            //создаём экземпляр класса Program
            object obj = Activator.CreateInstance(t); //MyAssembly.Program obj = new MyAssembly.Program();
            //получаем метод GetResult
            MethodInfo method = t.GetMethod("GetResult"); //obj.GetResult();
            //вызываем метод, передаём ему значения для параметров и получаем результат
            object result = method.Invoke(obj, new object[] { 6, 100, 3 });
            Console.WriteLine(result);              
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        Console.WriteLine("Press any key...");
        Console.ReadKey();
    }
    */
        #endregion
        #region Исследование типов
        static void Main(string[] args)
        {
            //Использования GetType для описания инфорации о типе:
            int A = 42;
            System.Type type = A.GetType();
            System.Console.WriteLine(type);
            //Использование рефлексии для получения информации из ассемблера
            System.Reflection.Assembly info = typeof(System.Int32).Assembly;
            System.Console.WriteLine(info);
            Type myType = typeof(Player);
            Console.WriteLine(myType.ToString());
            Console.WriteLine("Cтруктура типа:");
            foreach (MemberInfo mi in myType.GetMembers())
            {
                Console.WriteLine(mi.DeclaringType + " " + mi.MemberType + " " + mi.Name);
            }
            Console.WriteLine("Методы:");
            foreach (MethodInfo method in myType.GetMethods())
            {
                string modificator = "";
                if (method.IsStatic)
                    modificator += "static ";
                if (method.IsVirtual)
                    modificator += "virtual ";
                Console.Write(modificator + method.ReturnType.Name + " " + method.Name + " (");
                //получаем все параметры
                ParameterInfo[] parameters = method.GetParameters();
                for (int i = 0; i < parameters.Length; i++)
                {
                    Console.Write(parameters[i].ParameterType.Name + " " + parameters[i].Name);
                    if (i + 1 < parameters.Length) Console.Write(", ");
                }
                Console.WriteLine(")");
            }
            Console.WriteLine("Конструкторы:");
            foreach (ConstructorInfo ctor in myType.GetConstructors())
            {
                Console.Write(myType.Name + " (");
                // получаем параметры конструктора
                ParameterInfo[] parameters = ctor.GetParameters();
                for (int i = 0; i < parameters.Length; i++)
                {
                    Console.Write(parameters[i].ParameterType.Name + " " + parameters[i].Name);
                    if (i + 1 < parameters.Length) Console.Write(", ");
                }
                Console.WriteLine(")");
            }
            Console.WriteLine("Press any key...");
            Console.ReadLine();
        }
        #endregion
    }
}
