﻿using System;

namespace AttributeReflectionTest
{
    public class Player
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Player(string n, int a)
        {
            Name = n;
            Age = a;
        }
        public void Display()
        {
            Console.WriteLine("Имя: {0}  Возраст: {1}");
        }
        public int Payment(int hours, int perhour)
        {
            return hours * perhour;
        }
    }
}