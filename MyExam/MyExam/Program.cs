﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExam
{
    class Program
    {
        static void Main(string[] args)
        {
            MyProduct bread = new MyProduct("white bread", 12.60);
            Console.WriteLine(bread.ToString());
        }
    }
}
