﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SerializeTest
{
    public class MyClass
    {
        // Это будет элементом в файле XML
        [XmlElement("Name")]
        public string Name { get; set; }
        // Это будет атрибутом в файле XML
        [XmlAttribute("Value")]
        public string Value { get; set; }
        // Это поле мы не хотим сериализовать/десериализовать
        [XmlIgnore]
        public string ServiceField { get; set; }
    }
    //public class MyClassCollection
    //{
    //    [XmlArray("Collection"), XmlArrayItem("Item")]
    //    public List<MyClass> Collection { get; set; }
    //}

    [Serializable]
    public class UserInfo
    {
        public string UserName;
        public int UserPassword;
    }
}
