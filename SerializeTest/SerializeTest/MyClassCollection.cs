﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SerializeTest
{
     public class MyClassCollection
    {[XmlArray("Collection"), XmlArrayItem ("Item")]
    public List<MyClass> Collection { get; set; }

    }
}
