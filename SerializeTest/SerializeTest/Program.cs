﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SerializeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Test 1 Xml Serialization");
            var MyClassCollection = new MyClassCollection
            {
                Collection = new List<MyClass> {
            new MyClass {Name="name1", Value="val1",ServiceField="bla bla" },
            new MyClass {Name="name2", Value="val2",ServiceField="bla bla" },
            new MyClass {Name="name3", Value="val3",ServiceField="bla bla" },
            new MyClass {Name="name4", Value="val4",ServiceField="bla bla" }
            }
            };
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(MyClassCollection));
            StringWriter stringWriter = new StringWriter();
            xmlSerializer.Serialize(stringWriter, MyClassCollection);
            //myClasCollection -наши данные
            //После чего получаем xml в виде строки из потока:
            string xml = stringWriter.ToString();
            //и десериализуем
            var serializeData = xml;
            var stringReader = new StringReader(serializeData);
            MyClassCollection collection = (MyClassCollection)xmlSerializer.Deserialize(stringReader);
            foreach(MyClass mc in collection.Collection){
                Console.WriteLine("Name={0},Value={1},Field={2}", mc.Name, mc.Value, mc.ServiceField);

            }
            #region Serialization.Formatters.Binary
            Console.WriteLine("Test Serialization.Formatters.Binary");
            UserInfo userData = new UserInfo();
            userData.UserName = "Bob";
            userData.UserPassword = 123;
            // BinaryFormatter сохраняет данные в двоичном формате. Чтобы получить доступ к BinaryFormatter, понадобится
            // импортировать System.Runtime.Serialization.Formatters.Binary
            BinaryFormatter binFormat = new BinaryFormatter();
            // Сохранить объект в локальном файле.
            using (Stream fStream = new FileStream("user.dat",
               FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormat.Serialize(fStream, userData);
            }
            LoadUserInfoFromBinaryFile("user.dat");
            Console.WriteLine("Press any key");
            Console.ReadKey();
            #endregion
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
        static void LoadUserInfoFromBinaryFile(string fileName)
        {
            BinaryFormatter binFormat = new BinaryFormatter();

            using (Stream fStream = File.OpenRead(fileName))
            {
                UserInfo userFromDisk =
                     (UserInfo)binFormat.Deserialize(fStream);
                Console.WriteLine("{0},pass {1}", userFromDisk.UserName, userFromDisk.UserPassword);
            }
        }
    }
}
