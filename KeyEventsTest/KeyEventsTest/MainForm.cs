﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyEventsTest
{
    public partial class MainForm : Form
    {
        private readonly object lblTestKeyDown;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {
            int X = e.X;
            int Y = e.Y;//координаты мыши
            lbl1.Text = "MouseDown x= " + ";y = " + e.Y.ToString();
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.FillEllipse(Brushes.Green, X, Y, 10, 10);
            this.Text = "Buttons: " + e.Button.ToString();

        }

        private void MainForm_MouseUp(object sender, MouseEventArgs e)
        {
            lbl2.Text = "MouseUp x= " + e.ToString() + ";y = " + e.Y.ToString();
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.DrawEllipse(Pens.Red, e.X, e.Y, 10, 10);
        }

        private void MainForm_MouseMove(object sender, MouseEventArgs e)
        {
            int X = e.X;
            int Y = e.Y;
          

            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.DrawLine(Pens.Red, e.X, e.Y, 10, 10);
        }

        private void txtBoxKeyPress_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Выводит символ с клавиши
            lblTestPress.Text = " Presed key is: " + e.KeyChar.ToString();
        }

        private void txtBoxKeDown_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBoxKeDown_KeyDown(object sender, KeyEventArgs e)
        {//реагирует на нажатие любых клавиш, выводит ее код клавиатуры
            lblTestDown.Text = " key kod is: " + e.KeyValue.ToString();
        }

        private void PressMe_MouseMove(object sender, MouseEventArgs e)
        {
            Random rnd = new Random();
            int n = rnd.Next(10, 123);
            PressMe.Left = n;
            PressMe.Top = n + 10;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = (char)0;//символ с кодом 0-пустота
        }
    }
}
