﻿namespace KeyEventsTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl1 = new System.Windows.Forms.Label();
            this.lblTestPress = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.txtBoxKeDown = new System.Windows.Forms.TextBox();
            this.txtBoxKeyPress = new System.Windows.Forms.TextBox();
            this.lblTestDown = new System.Windows.Forms.Label();
            this.PressMe = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(131, 38);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(75, 13);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Mouse Events";
            // 
            // lblTestPress
            // 
            this.lblTestPress.AutoSize = true;
            this.lblTestPress.Location = new System.Drawing.Point(52, 302);
            this.lblTestPress.Name = "lblTestPress";
            this.lblTestPress.Size = new System.Drawing.Size(78, 13);
            this.lblTestPress.TabIndex = 1;
            this.lblTestPress.Text = "Test Key Press";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Location = new System.Drawing.Point(413, 37);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(75, 13);
            this.lbl2.TabIndex = 5;
            this.lbl2.Text = "Mouse Events";
            // 
            // txtBoxKeDown
            // 
            this.txtBoxKeDown.Location = new System.Drawing.Point(26, 432);
            this.txtBoxKeDown.Name = "txtBoxKeDown";
            this.txtBoxKeDown.Size = new System.Drawing.Size(100, 20);
            this.txtBoxKeDown.TabIndex = 6;
            this.txtBoxKeDown.TextChanged += new System.EventHandler(this.txtBoxKeDown_TextChanged);
            this.txtBoxKeDown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBoxKeDown_KeyDown);
            // 
            // txtBoxKeyPress
            // 
            this.txtBoxKeyPress.Location = new System.Drawing.Point(26, 345);
            this.txtBoxKeyPress.Name = "txtBoxKeyPress";
            this.txtBoxKeyPress.Size = new System.Drawing.Size(100, 20);
            this.txtBoxKeyPress.TabIndex = 7;
            this.txtBoxKeyPress.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBoxKeyPress_KeyPress);
            // 
            // lblTestDown
            // 
            this.lblTestDown.AutoSize = true;
            this.lblTestDown.Location = new System.Drawing.Point(52, 393);
            this.lblTestDown.Name = "lblTestDown";
            this.lblTestDown.Size = new System.Drawing.Size(80, 13);
            this.lblTestDown.TabIndex = 8;
            this.lblTestDown.Text = "Test Key Down";
            // 
            // PressMe
            // 
            this.PressMe.Location = new System.Drawing.Point(618, 461);
            this.PressMe.Name = "PressMe";
            this.PressMe.Size = new System.Drawing.Size(75, 23);
            this.PressMe.TabIndex = 9;
            this.PressMe.Text = "PressMe";
            this.PressMe.UseVisualStyleBackColor = true;
            this.PressMe.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PressMe_MouseMove);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(491, 257);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 10;
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 500);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.PressMe);
            this.Controls.Add(this.lblTestDown);
            this.Controls.Add(this.txtBoxKeyPress);
            this.Controls.Add(this.txtBoxKeDown);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lblTestPress);
            this.Controls.Add(this.lbl1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lblTestPress;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.TextBox txtBoxKeDown;
        private System.Windows.Forms.TextBox txtBoxKeyPress;
        private System.Windows.Forms.Label lblTestDown;
        private System.Windows.Forms.Button PressMe;
        private System.Windows.Forms.TextBox textBox1;
    }
}

