﻿using StoredValues.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoredValues
{
    public partial class MainForm : Form
    { private Settings settings = Settings.Default;
        public MainForm()
        {
            settings.Reload();//Загрузка сохраненных значений компонентов в settings
            InitializeComponent();
            //Применить загруженные значения  к  компонентам
            InitializeMyComponents();
        }
        private void InitializeMyComponents() {
            //this.Text = settings.IntValue.ToString();
            chTest.Checked = settings.BoolValue;
            textInput.Text = settings.TextValue;
            this.Text = settings.IntValue.ToString();

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //e.Cancel = true;//окно не закроется
            SaveSettings();
        }
        private void SaveSettings() {
            settings.TextValue = textInput.Text;
            settings.BoolValue = chTest.Checked;
            settings.IntValue = int.Parse(this.Text);
            settings.Save();
        }
    }
}
