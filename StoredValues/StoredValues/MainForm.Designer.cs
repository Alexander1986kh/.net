﻿namespace StoredValues
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textInput = new System.Windows.Forms.TextBox();
            this.chTest = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // textInput
            // 
            this.textInput.Location = new System.Drawing.Point(116, 76);
            this.textInput.Name = "textInput";
            this.textInput.Size = new System.Drawing.Size(171, 20);
            this.textInput.TabIndex = 0;
            // 
            // chTest
            // 
            this.chTest.AutoSize = true;
            this.chTest.BackColor = System.Drawing.SystemColors.ControlDark;
            this.chTest.Location = new System.Drawing.Point(116, 153);
            this.chTest.Name = "chTest";
            this.chTest.Size = new System.Drawing.Size(65, 17);
            this.chTest.TabIndex = 1;
            this.chTest.Text = "Да/Нет";
            this.chTest.UseVisualStyleBackColor = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 507);
            this.Controls.Add(this.chTest);
            this.Controls.Add(this.textInput);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textInput;
        private System.Windows.Forms.CheckBox chTest;
    }
}

