--use [sqltest]
--select 
--[dbo].[PC].model,
--[dbo].[PC].price 
--from [dbo].[PC]
-- left join [dbo].[Product] on [dbo].[Product].model=[dbo].[PC].[model]
-- union all--��� �� ���������� �����������
-- select 
-- [dbo].[Printer].[model], 
-- [dbo].[Printer].[price] 
-- from [dbo].[Printer]
-- order by [price]-- ��� ������� ������������(���������� �� ����)

 use [sqltest]
go
select [dbo].[Product].maker,[dbo].[PC].price, 'PC' as PRODUCT_NAME from [dbo].PC 
left join [dbo].Product on [dbo].[Product].model=[dbo].PC.model
union all
select [dbo].[Product].maker, [dbo].Printer.price, 'Printer' as PRODUCT_NAME from [dbo].Printer
left join [dbo].Product on [dbo].[Product].model=[dbo].Printer.model
order by 2
SELECT AVG (price) AS PriceAvg FROM [dbo].[Printer];--������� �������� �������� �� ������� (price) ������� [dbo].[Printer]
SELECT MAX (price) AS PriceAvg FROM [dbo].[Printer];--������� ������������� �������� �� ������� (price) ������� [dbo].[Printer]
SELECT AVG (price) AS PriceAvgColorPrinter FROM [dbo].[Printer]
where [dbo].[Printer].color = 'y'; --�������� �������� �� ������� (price) ������� [dbo].[Printer],����� ������� �������
SELECT AVG (price) AS PriceAvgLaserPrinter FROM [dbo].[Printer]
where [dbo].[Printer].type = 'Laser'; --�������� �������� �� ������� (price) ������� [dbo].[Printer],����� ������� ��������
SELECT AVG (price) AS PriceAvgLaserPrinter FROM [dbo].[Printer]
where [dbo].[Printer].type = 'Jet'; --�������� �������� �� ������� (price) ������� [dbo].[Printer],����� ������� ��������
SELECT
[dbo].[Printer].type, AVG (price) AS PriceAvgMatrixPrinter  FROM [dbo].[Printer] where [dbo].[Printer].type = 'Matrix'
group by [dbo].[Printer].[type];-- ���������� ������� �� ��������  type 
 --�������� �������� �� ������� (price) ������� [dbo].[Printer],����� ������� ���������
  --������� ������� ��������� ������ � ��������
 SELECT
 'Printer' as Product_type,
 AVG (Printer.price) AS AveragePrice 
 FROM 
 [dbo].[Printer] as Printer
 union-- ����������� ���������� �������� 
 select
  'Laptop',
  AVG (Laptop.price)
 FROM 
 [dbo].[Laptop] as Laptop
 union
  select
   'PC' ,
  AVG (PC.price)
 FROM 
 [dbo].[PC] as PC
