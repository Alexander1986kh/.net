﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullableDbNullTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int? x = 10;//ссылочный тип, способный хранить null
            int? y = 10;
            if (x.HasValue)
            {
                Console.WriteLine("{0}", x.Value);
            }
            else
            {
                Console.WriteLine("Undefined");
            }
            #region Иная запись условия  
            if (y != null)
            {
                Console.WriteLine("{0}", y);
            }
            else
            {
                Console.WriteLine("Undefined");
            }
            #endregion
            #region Преобразование типов
            //int? n = null;
            //int m1 = n;//Даже не скомпилируется
            //неявное приведение типов не работает
            //int m2 = (int)n;//Получим исключительную ситуацию (System.InvalidOperationException) 
            //во время выполнения программы (явное приведение типа)
            //int m3 = n.Value;
            #endregion
            #region Сравнение типов
            int? num1 = 10;
            int? num2 = null;
            if(num1 >= num2)//обратное условие num1 < num2 также выдаст false
            {
                Console.WriteLine("num1 is greater than or equal to num2");
            }
            else
            {
                //выполнится эта ветка, но значение в num1 не меньше значения num2
                Console.WriteLine("num1 >= num2 returned false (but num1 < num2 also is false)");
            }
            num1 = null;
            if (num1 == num2)
            {
                //два значения null неразличимы (равны друг другу)
                Console.WriteLine("num1 == num2, returns true when value of both is null");
            }
            else
            {
                Console.WriteLine("num1 is not equal to num2");
            }
            num1 = 10;
            if (num1 != num2)
            {
                Console.WriteLine("num1 is not equal to num2");
            }
            else
            {
                Console.WriteLine("num1 is equal to num2");
            }
            //null - несравнимая величина (сравнима только с другим null)
            #endregion
            #region Упаковка
            int? c = null;
            //d = c, есди значение c - null, то в переменную значимого типа d запишем -1
            int d = c ?? -1;
            //упаковка в ссылочный тип
            //boxing(упаковка) - процесс преобразования в тип, совместимый с object
            bool? b = null;//Now o is null
            object o = b;
            b = false;
            int? i = 44;
            object bBoxed = b;//bBoxed contains a b
            object IBoxed = i;//iBoxed contains a i
            int Xx = 10;//int - это 4-и байта
            object xBoxed = Xx;//object - это 10 байт
            #region Примеры неявной упаковки значимых типов
            int p = 15;
            Print(p);//произошла автоматическая неявная упаковка
            ArrayList list = new ArrayList();
            list.Add(p);//произошла автоматическая неявная упаковка
            #endregion
            #endregion
            string connectionString = @"Data Source=PC36-8-Z;Initial Catalog=db28pr8;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[CUSTOMERS]", con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string nick = reader["NICK_NAME"].ToString();
                        //если пришло null, в .NET это тип System.DBNull
                        //и вызвался его ToString()
                        string phone = reader["PHONE"].ToString();
                        string email = reader["EMAIL"].ToString();
                        string id_men2 = (string)reader["ID_MEN"];//если столбец содержит null
                        //в .NET это System.DBNull явное приведение невозможно
                        string id_men = String.Empty;
                    }
                }
            }
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
        public static void Print(object obj)
        {
            Console.WriteLine(obj);
        }
    }
    public static class MyExtensions
    {//преобразовывает System.Object в любой тип T
        public static T ConvertFromDBVal<T>(object obj)
        {
            if (obj == null||obj==DBNull.Value)
            {
                return default(T);
            }
            else
            {
                return (T)obj;
                //если ynot tull , то явноле приведение типов
                // аналог (int)obj или (double)obj
            }
        }

    }
}
