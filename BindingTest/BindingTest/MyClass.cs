﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BindingTest
{
    class MyClass
    {
        private string info;//field
        //на поле- даже открытое(public) нельзя сделать привязку(Binding)
        public string Info//однонаправленная привязка
        {
            get
            {
                return this.info;
            }

            set
            {
                this.info = value;
            }
        }
        public MyClass(string info)
        {
            this.info = info;
        }
        protected MyClass()
        {
            this.info = string.Empty;
        }
        //
    }
}
