﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BindingTest
{
    public partial class Form1 : Form
    {
        private MyClass myClass;
        public Form1()
        {
            InitializeComponent();
            //lbl1.DataBindings.Add(new Binding("Text", txtInput, "Text", true));
            Binding binding = new Binding("Text",//свойство Target, которое отображает
                txtInput,
                "Text",//свойство-источник данных  компонента Source
                true);// применить привязку немедленно, как пришли данные в Source
            lbl1.DataBindings.Add(binding);
            //lbl1- Label, в которыйидут данныее- Target
            //txtInput -textBox-компонент- источник данных-Source
            this.myClass = new MyClass("my value from my class");
            lbl2.DataBindings.Add(new Binding("Text", myClass,
                "Info",//имя public свойства(но от регистра не зависит)
                true));//однонаправленная привязка
        }
    }
}
