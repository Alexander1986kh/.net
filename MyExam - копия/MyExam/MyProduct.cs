﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExam
{
    class MyProduct: INotifyPropertyChanged
    {
        string name;
        double price;

        public event PropertyChangedEventHandler PropertyChanged;//Создание события
        private void NotifyPropertyChanged(String info)//Создание метода обработчика события
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }

            set
            {if (name != value)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public double Price
        {
            get
            {
                return price;
            }

            set
            {
                {
                    if (price != value)
                    {
                        price = value;
                        NotifyPropertyChanged("Price");
                    }
                }
            }
        }
        #endregion
        #region Constructors
        private MyProduct() { }
        public MyProduct(string aname, double aprice)
        {
            name = aname;
            price = aprice;
        }
        #endregion
        #region унаследованные obect-методы
        public bool Equals(MyProduct product)
        {
            if (product == null)
                return false;
            if (this.name == product.name && this.price == product.price)
                return true;
            else return false;

        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            MyProduct product = obj as MyProduct;
            if (product == null)
                return false;
            else
                return this.Equals(product);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return String.Format("Наименование товара:{0}, Цена: {1}", this.name, this.price);
        }
        #endregion
    }
}
