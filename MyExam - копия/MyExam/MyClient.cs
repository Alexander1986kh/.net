﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExam
{
    class MyClient
    {
        string name;
        string phone;
        string email;
        MyAccount[] accounts;
        int age;
        string regionInfo;
        #region Properties
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }

            set
            {
                phone = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        internal MyAccount[] Accounts
        {
            get
            {
                return accounts;
            }

            set
            {
                accounts = value;
            }
        }

        public int Age
        {
            get
            {
                return age;
            }

            set
            {
                age = value;
            }
        }

        public string RegionInfo
        {
            get
            {
                return regionInfo;
            }

            set
            {
                regionInfo = value;
            }
        }
        #endregion
        #region Constructors
        private MyClient() { }
        public MyClient(string aname,string aphone,string aemail, MyAccount[] argaccounts,int argage,string aregionInfo)
        {
            name = aname;
            phone = aphone;
            email = aemail;
            accounts = argaccounts;
            age = argage;
            regionInfo = aregionInfo;
        }
        #endregion
        #region override objects metods
        public override string ToString()
        {
            return String.Format("Имя клиента: {0},Телефон: {1}, Имэйл: {2}", this.name, this.phone, this.email);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public bool Equals(MyClient client)
        {
            if (client == null)
                return false;
            if (this.name == client.name&&this.phone==client.phone&&this.email==client.email)
                return true;
            else return false;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            MyClient client = obj as MyClient;
            if (client == null)
                return false;
            else
                return this.Equals(client);
        }
        #endregion
    }
}
