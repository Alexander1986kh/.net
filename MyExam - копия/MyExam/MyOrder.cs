﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExam
{
    class MyOrder
    {
        private string description;
        private string ordersDate;
        private double totalCosts;
        private MyClient client;
        private  MyOrderPosition[] mop;
        #region Properties
        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        public string OrdersDate
        {
            get
            {
                return ordersDate;
            }

            set
            {
                ordersDate = value;
            }
        }

        public double TotalCosts
        {
            get
            {
                return totalCosts;
            }

            set
            {
                totalCosts = value;
            }
        }

        internal MyClient Client
        {
            get
            {
                return client;
            }

            set
            {
                client = value;
            }
        }

        internal MyOrderPosition[] Mop
        {
            get
            {
                return mop;
            }

            set
            {
                mop = value;
            }
        }
        #endregion
        #region Constructors
        private MyOrder() { }
        public MyOrder(string adecription, string aordersDate, double atotalcosts, MyClient aclient, MyOrderPosition[] amop)
        {
            description = adecription;
            ordersDate = aordersDate;
            totalCosts = atotalcosts;
            client = aclient;
            mop = amop;
        }
        #endregion
        #region override objects metods
        public override string ToString()
        {
            return String.Format("Информация о заказе: {0},Дата заказа: {1}, Сумма: {2}", 
                this.description, this.ordersDate, this.totalCosts);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public bool Equals(MyOrder order)
        {
            if (client == null)
                return false;
            if (this.description == order.description && this.ordersDate == order.ordersDate && this.totalCosts == order.totalCosts)
                return true;
            else return false;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            MyOrder order = obj as MyOrder;
            if (order == null)
                return false;
            else
                return this.Equals(order);
        }
        #endregion
    }
}
