﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExam
{
    class MyAccount
    {
        private MyClient client;
        private double account;
        private string accountNumber;
        #region Properties
        internal MyClient Client
        {
            get
            {
                return client;
            }

            set
            {
                client = value;
            }
        }

        public double Account
        {
            get
            {
                return account;
            }

            set
            {
                account = value;
            }
        }

        public string AccountNumber
        {
            get
            {
                return accountNumber;
            }

            set
            {
                accountNumber = value;
            }
        }
        #endregion
        #region Constructors
        private MyAccount() { }
        public MyAccount(MyClient aclient, double aacount, string aaccountNumber)
        {
            client = aclient;
            account = aacount;
            accountNumber = aaccountNumber;
        }
        #endregion
        #region override objects metods
        public override string ToString()
        {
            return String.Format("Клиент: {0}, Денег на счеу: {1}, Номер счета: {2}", this.client.ToString(),
                this.account, this.accountNumber);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public bool Equals(MyAccount ac)
        {
            if (ac == null)
                return false;
            if (this.accountNumber == ac.accountNumber)
                return true;
            else return false;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            MyAccount ac = obj as MyAccount;
            if (ac == null)
                return false;
            else
                return this.Equals(ac);
        }
        #endregion
    }
}
