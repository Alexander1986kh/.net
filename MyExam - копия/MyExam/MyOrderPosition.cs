﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExam
{
    class MyOrderPosition
    {
        MyProduct product;
        double price;
        int count;//количество заканных товаров этого вида
        MyOrder order;
        MyClient client;
        #region Properties
        internal MyProduct Product
        {
            get
            {
                return product;
            }

            set
            {
                product = value;
            }
        }

        public double Price
        {
            get
            {
                return price;
            }

            set
            {
                price = value;
            }
        }

        public int Count
        {
            get
            {
                return count;
            }

            set
            {
                count = value;
            }
        }

        internal MyOrder Order
        {
            get
            {
                return order;
            }

            set
            {
                order = value;
            }
        }

        internal MyClient Client
        {
            get
            {
                return client;
            }

            set
            {
                client = value;
            }
        }
        #endregion
        #region Constructors
        private MyOrderPosition() { }
        public MyOrderPosition(MyProduct aproduct,double aprice,int acount,MyOrder aorder,MyClient aclient)
        {
            product = aproduct;
            price = aprice;
            count = acount;
            order = aorder;
            client = aclient;
        }
        #endregion
        #region унаследованные obect-методы
        public bool Equals(MyOrderPosition mop)
        {
            if (product == null)
                return false;
            if (this.product.Equals(mop.product)&& this.price == mop.price&&this.count==mop.count)
                return true;
            else return false;

        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            MyOrderPosition mop = obj as MyOrderPosition;
            if (mop == null)
                return false;
            else
                return this.Equals(mop);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return String.Format("Наименование товара:{0}, Цена: {1}, Количество заканных товаров этого вида{2}",
                this.product.Name, this.price, this.count);
        }
        #endregion
    }
}
