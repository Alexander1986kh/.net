﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLoggerTestConsole
{
    class LogNames 

    {
        private string[] mylogNames;
        private bool use_c;
        private bool use_d;
        private bool use_e;
        private bool use_f;

        #region Properties
        public string[] myLogNames
        {
            get
            {
                return mylogNames;
            }

            set
            {
                mylogNames = value;

            }
        }

        public bool Use_c
        {
            get
            {
                return use_c;
            }

            set
            {
                use_c = value;
                use_d = !value;
                use_e = !value;
                use_f = !value;
            }
        }

        public bool Use_d
        {
            get
            {
                return use_d;
            }

            set
            {
                use_d = value;
                use_c = !value;
                use_e = !value;
                use_f = !value;
            }
        }

        public bool Use_e
        {
            get
            {
                return use_e;
            }

            set
            {
                use_e = value;
                use_d = !value;
                use_c = !value;
                use_f = !value;
            }
        }

        public bool Use_f
        {
            get
            {
                return use_f;
            }

            set
            {
                use_f = value;
                use_d = !value;
                use_e = !value;
                use_c = !value;
            }
        }
        #endregion

        public LogNames() {
            string[] logNames = new string[] {
                 @"c:\mylog.txt", @"d:\mylog.txt", @"e:\mylog.txt", @"f:\mylog.txt" };
            use_c = true;
            use_d = false;
            use_e = false;
            use_f = false;
        }
      
        public string this[int index]
        {
            get { return mylogNames[index]; }
        }
        public string this[string str]
        {
           get {
                if(str!="c"&&str!="C"&&str!="D"&&str!="d"&&str!="e"&&str!="E"&&str!="f"&&str!="F")
                 throw new ArgumentException("Правильно выберите директорию");
                string res = (str == "c" || str == "C") ? mylogNames[0] :
                             (str == "d" || str == "D") ? mylogNames[1] :
                                (str == "e" || str == "E") ? mylogNames[2] :
                                     mylogNames[3];
                return res;                                        
                }
            
        }
     
    }
}
