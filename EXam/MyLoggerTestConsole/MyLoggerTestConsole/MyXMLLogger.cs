﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml; 

namespace MyLoggerTestConsole
{
    class MyXMLLogger : IMylogger,IDisposable
    {
        string file_name;//путь к файлу
        XmlReader reader;//инициализация и открытие-закрытие по месту использования
        XmlWriter writer;

        public string File_name
        {
            get
            {
                return file_name;
            }

            set
            {
                file_name = value;
            }
        }
        private MyXMLLogger() { }
        public  MyXMLLogger(string afile_name)
        {
            file_name = afile_name;
        }

        public void Dispose()//Дает рекомендацию сборщику мусора для очистки памяти
            //ручной запуск
        {
            reader.Dispose();
            writer.Dispose();
        }
        void IDisposable.Dispose()//Дает рекомендацию сборщику мусора для очистки памяти
            // автоматический запуск
        {
            reader.Dispose();
            writer.Dispose();
        }

        public void WriteProtocol(string date_time, string action, string cod_line,
            string log_txt)
        {
            writer = XmlWriter.Create(file_name);
            //Статический метод Create общий для всех экземпляров , внутри него отсутстсвует this
            //вызывается  у класса без экземпляра
           
            
                writer.WriteStartDocument();
                writer.WriteRaw("\n");
                writer.WriteStartElement("log");
                writer.WriteRaw("\n");
                writer.WriteStartElement("log_row");
                writer.WriteAttributeString("rownum", "1");
                writer.WriteAttributeString("date_time", date_time);
                writer.WriteAttributeString("action", action);
                writer.WriteAttributeString("cod_line", cod_line);
                writer.WriteRaw(log_txt);
                writer.WriteRaw("\n");
                writer.Close();
      
        }
     
    }
}
