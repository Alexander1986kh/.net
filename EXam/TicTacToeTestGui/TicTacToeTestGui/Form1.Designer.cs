﻿namespace TicTacToeTestGui
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn11 = new System.Windows.Forms.Button();
            this.btn12 = new System.Windows.Forms.Button();
            this.btn13 = new System.Windows.Forms.Button();
            this.btn21 = new System.Windows.Forms.Button();
            this.btn22 = new System.Windows.Forms.Button();
            this.btn23 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn11
            // 
            this.btn11.Location = new System.Drawing.Point(105, 36);
            this.btn11.Name = "btn11";
            this.btn11.Size = new System.Drawing.Size(96, 23);
            this.btn11.TabIndex = 0;
            this.btn11.Text = "11";
            this.btn11.UseVisualStyleBackColor = true;
            this.btn11.Click += new System.EventHandler(this.btn11_Click);
            // 
            // btn12
            // 
            this.btn12.Location = new System.Drawing.Point(234, 36);
            this.btn12.Name = "btn12";
            this.btn12.Size = new System.Drawing.Size(96, 23);
            this.btn12.TabIndex = 1;
            this.btn12.Text = "12";
            this.btn12.UseVisualStyleBackColor = true;
            this.btn12.Click += new System.EventHandler(this.btn12_Click);
            // 
            // btn13
            // 
            this.btn13.Location = new System.Drawing.Point(371, 35);
            this.btn13.Name = "btn13";
            this.btn13.Size = new System.Drawing.Size(95, 23);
            this.btn13.TabIndex = 2;
            this.btn13.Text = "13";
            this.btn13.UseVisualStyleBackColor = true;
            this.btn13.Click += new System.EventHandler(this.btn13_Click);
            // 
            // btn21
            // 
            this.btn21.Location = new System.Drawing.Point(105, 77);
            this.btn21.Name = "btn21";
            this.btn21.Size = new System.Drawing.Size(96, 23);
            this.btn21.TabIndex = 3;
            this.btn21.Text = "21";
            this.btn21.UseVisualStyleBackColor = true;
            this.btn21.Click += new System.EventHandler(this.btn13_Click);
            // 
            // btn22
            // 
            this.btn22.Location = new System.Drawing.Point(234, 77);
            this.btn22.Name = "btn22";
            this.btn22.Size = new System.Drawing.Size(96, 23);
            this.btn22.TabIndex = 4;
            this.btn22.Text = "22";
            this.btn22.UseVisualStyleBackColor = true;
            this.btn22.Click += new System.EventHandler(this.btn13_Click);
            // 
            // btn23
            // 
            this.btn23.Location = new System.Drawing.Point(371, 77);
            this.btn23.Name = "btn23";
            this.btn23.Size = new System.Drawing.Size(95, 23);
            this.btn23.TabIndex = 5;
            this.btn23.Text = "23";
            this.btn23.UseVisualStyleBackColor = true;
            this.btn23.Click += new System.EventHandler(this.btn13_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 303);
            this.Controls.Add(this.btn23);
            this.Controls.Add(this.btn22);
            this.Controls.Add(this.btn21);
            this.Controls.Add(this.btn13);
            this.Controls.Add(this.btn12);
            this.Controls.Add(this.btn11);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn11;
        private System.Windows.Forms.Button btn12;
        private System.Windows.Forms.Button btn13;
        private System.Windows.Forms.Button btn21;
        private System.Windows.Forms.Button btn22;
        private System.Windows.Forms.Button btn23;
    }
}

