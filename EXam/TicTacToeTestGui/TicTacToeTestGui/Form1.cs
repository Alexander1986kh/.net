﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeTestGui
{
    public partial class Form1 : Form
    {
        private Button btn31;
        public Form1()
        {
            InitializeComponent();
            MYInitializeComponent();
        }

        private void MYInitializeComponent() { 
            // 
            // btn31
            // 
            btn31 = new Button();
            this.btn31.Location = new System.Drawing.Point(19, 128);
            this.btn31.Name = "btn31";
            this.btn31.Size = new System.Drawing.Size(75, 47);
            this.btn31.TabIndex = 3;
            this.btn31.Text = "31";
            this.btn31.UseVisualStyleBackColor = true;
            this.btn31.Click += new System.EventHandler(this.btn13_Click);
            this.Controls.Add(this.btn31);
        }
        private void btn11_Click(object sender, EventArgs e)
        {
            btn11.Text = "Clicked!";

        }

        private void btn12_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;//явное преобразование типов в С
            btn.Text = "Checked!";
        }

        private void btn13_Click(object sender, EventArgs e)
        {
            if(sender is Button)
            {
                Button btn = sender as Button;//вернет NULL, есле sender не кнопка
                if (btn != null)
                {
                    btn.Text = "Clicked:"+btn.Name;
                }
            }
        }
    }
}
