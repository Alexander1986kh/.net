﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSequenceTestControl
{
    class Program
    {
        static void Main(string[] args)
        {
            FibonacciSequence myfibonacci = new FibonacciSequence(7);
            foreach(int f in myfibonacci)
            {
                Console.Write(f+" ");
            }
            Console.Write("\n");
            foreach(int f in myfibonacci.GetFirst(3))
            {
                Console.Write(f + " ");
            }
        }
    }
}
