﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSequenceTestControl
{
    class FibonacciSequence : IEnumerator, IEnumerable
    {
        int[] fibonacci;
        int index = -1;
        private FibonacciSequence() {
            fibonacci = new int[] { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987 };
        }
        public FibonacciSequence(int size)
        {
            fibonacci = new int[size];
            fibonacci[0] = 1;
            if (size > 1)
                fibonacci[1] = 1;
            for (int i = 2; i < size; i++)
            {
                fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1];
            }
        }

        internal IEnumerable<int> GetFirst(int number)
        { 
            for (int i = 0; i < number; i++)
            {
                yield return fibonacci[i];
            };
        }
        #region Реализация IEnumerator

        public object Current// foreach - только чтение
        {
            get { return fibonacci[index]; }
        }
        public bool MoveNext()
        {
            if (index == fibonacci.Length - 1)
            {
                Reset(); //нет следующего 
                return false;
            }
            index++; //сдвинулись на одни
            return true; //следующий есть
            //и текущий на него настроен
        }
        public void Reset()
        {
            index = -1;
        }
        #endregion
        #region Реализация IEnumerable
        public IEnumerator GetEnumerator()
        {
            return this;
        }
        #endregion
        
    }
}
