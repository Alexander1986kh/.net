﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CesarEncoderDecoderTestControle
{
    class CesarEncoder
    {
        int step;//шаг смещения при кодировании

        public int Step
        {
            get
            {
                return step;
            }

            set
            {
                step = value;
            }
        }
        public CesarEncoder(int astep)
        {
            step = astep;
        }
        public CesarEncoder()
        {
            step = 0;//кодирования не произойдет
        }
        public void Encoder(string path_to_file)
        {
            using (FileStream
                    filestream = new FileStream(path_to_file, FileMode.Open))
            {
                byte[] mas = new byte[filestream.Length];// создаем массив байтов
                filestream.Read(mas, 0, mas.Length);//переписываем побайтно из файла в массив
                filestream.Seek(0, SeekOrigin.Begin);//переводим курсор в начало файла
                for (int i = 0; i < mas.Length; i++)
                {
                    int temp = 0;
                    temp = Convert.ToInt32(mas[i]) + step;//преобразуем байты в int,
                    //добавляем смещение
                    mas[i] = Convert.ToByte(temp);//int обратно в байты
                }
                filestream.Write(mas,0, mas.Length);//переписываем из массива в файл
                filestream.Close();
            }


        }
        public void Decode(string path_to_file)
        {
            using (FileStream
                    filestream = new FileStream(path_to_file, FileMode.Open))
            {
                byte[] mas = new byte[filestream.Length];
                filestream.Read(mas, 0, mas.Length);
                filestream.Seek(0, SeekOrigin.Begin);
                for (int i = 0; i < mas.Length; i++)
                {
                    int temp = 0;
                    temp = Convert.ToInt32(mas[i]) - step;
                    mas[i] = Convert.ToByte(temp);
                }
                filestream.Write(mas, 0, mas.Length);
                filestream.Close();
                //int[] mas = new int[filestream.Length];
                //for (int i = 0; i < filestream.Length; i++)
                //{
                //    if (filestream.ReadByte() == -1)
                //        break;
                //    mas[i] = filestream.ReadByte() - step;
                //    filestream.WriteByte((byte)mas[i]);
            
            }


        }
    }
}
