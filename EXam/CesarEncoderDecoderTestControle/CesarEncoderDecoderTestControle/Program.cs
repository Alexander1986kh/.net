﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CesarEncoderDecoderTestControle
{
    class Program
    {
        static void Main(string[] args)
        {
            CesarEncoder myces=new CesarEncoder(3);//смещение=3
            myces.Encoder(@"D:\mycesartest.txt");
            myces.Decode(@"D:\mycesartest.txt");
            CesarEncoder mycesar = new CesarEncoder();//смещение =0
            mycesar.Encoder(@"D:\mycesartest.txt");
            mycesar.Decode(@"D:\mycesartest.txt");
        }
    }
}
