﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeWinForms
{
    public partial class Form1 : Form
    {
        private Label[,] lbl_mas=new Label[3,3];
        public Form1()
        {
            //InitializeComponent();
            MyInitializeComponent();
        }
        private void MyInitializeComponent()
        {
            for(int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    lbl_mas[i,j] = new Label();
                    //this.lbl_mas[i,j].AutoSize = true;
                    this.lbl_mas[i, j].Size = new System.Drawing.Size(40, 40);
                    this.lbl_mas[i,j].ForeColor = System.Drawing.Color.DarkRed;
                    this.lbl_mas[i,j].Location = new System.Drawing.Point(40+i*50, 40+j*50);
                    // this.lbl_mas[i,j].Name = "lbl"+(1+i).ToString()+(1+j).ToString();
                    this.lbl_mas[i, j].Name = String.Format("lbl{1}{0}", i+1,j+1);
                    this.lbl_mas[i,j].TabIndex = 0;
                    this.lbl_mas[i,j].Text = (1+j).ToString()+(1 + i).ToString();
                    this.lbl_mas[i, j].Click += new EventHandler(lbls_click);//вешаем обработчик события на label
                    this.Controls.Add(this.lbl_mas[i,j]);

                }
            
            }
       
        }
        private void lbls_click(object sender, EventArgs e)
        {
            if(sender is Label)
            {
               Label mysender= sender as Label;
                if (mysender!=null)
                {
                    mysender.Text = mysender.Name;
                }
            }
        }
    }
}
