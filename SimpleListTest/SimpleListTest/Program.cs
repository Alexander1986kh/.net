﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleListTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SimpleList test = new SimpleList();
            Console.WriteLine("Populate this List");
            test.Add("one");
            test.Add("two");
            test.Add("three");
            test.Add("four");
            test.Add("five");
            test.Add("six");
            test.Add("seven");
            test.Add("eight");
            test.PrintContents();
            Console.WriteLine();
            Library lib = new Library();
            Console.WriteLine("Книги из библиотеки без реализации IEnumerator через for:");
            for (int i = 0; i < lib.Lenth; i++)
            {
                Console.WriteLine(lib[i]);
            }
            Console.WriteLine();
            Console.WriteLine("Книги из библиотеки с реализованным IEnumerator через foreach:");
            foreach (Book b in lib)
            {
                Console.WriteLine(b);
            }
            //Все книги
            //Итератор , только для чтения
            //Потокобезопасно и до 15% быстрее
            foreach (Book b in lib.GetBooks(3))
            {
                Console.WriteLine(b);
            }
            //Первые три книги из библиотеке
            //нагрузка на сборщик мусора
            Console.WriteLine();
            Console.WriteLine("Цифры фибоначчи:");
            FibonacciSequence fibonacci = new FibonacciSequence();
            foreach (int f in fibonacci)
            {
                Console.Write(f + " ");
            }
            Console.WriteLine();
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }

    public class Book
    {
        public string Name { get; set; }
        public override string ToString()
        {
            return String.Format("Книга - " + Name);
        }
    }
    /// <summary>
    /// класс-обёртка над массивом из Book
    /// </summary>
    public class Library : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            return this.books.GetEnumerator();
            //вернули реализацию нашего массива
            //Enumerator - это интерфейс Итератора
        }
        private Book[] books;
        public Library()
        {
            books = new Book[]
            {
                new Book { Name = "Книга 1"},
                new Book { Name = "Вторая Книга"},
                new Book { Name = "Букварь"}
            };
        }
        public int Lenth { get { return books.Length; } }
        public Book this[int index]
        {
            get { return books[index]; }
            set { books[index] = value; }
        }
        public IEnumerable GetBooks(int max)
        {
            {
                for (int i = 0; i < max; i++)
                {
                    if (i == books.Length)
                    {
                        yield break;//на каждом шаге кэширует экземпляр, на протяжени всего цикла 
                        // хранит все эземпляры
                    }
                    else
                    {
                        yield return books[i];
                    }
                }
            }
        }
    }
    class SimpleList : IList //для использования моего класса в списках
    {
        private object[] _contents = new object[8];
        //инициализация - до выполнение конструктора, но в момент его вызова через оператор new
        //private object[] _contents;
        private int _count;
        public SimpleList()
        {
            this._count = 0;
            //_contents = new object[this._count];
        }
        public void PrintContents()
        {
            Console.WriteLine("List has a capacity of {0} and currently has {1} elements.", _contents.Length, _count);
            Console.Write("List contents:");
            for (int i = 0; i < Count; i++)
            {
                Console.Write(" {0}", _contents[i]);
            }
            foreach (object _content in _contents)
            {
                Console.Write(" {0}", _content);
            }
            Console.WriteLine();
        }

        #region IList Members
        public int Add(object value)
        {
            if(_count < _contents.Length)
            {
                _contents[_count] = value;
                _count++;
                return (_count - 1);
            }
            else
            {
                return -1; //изменение размера и перезапись
                /*
             Если при добавлении элемента оказывается,
             что массив полностью заполнен,
             будет создан новый массив размером
             (n * 3) / 2 + 1,
             в него будут помещены все элементы
             из старого массива + новый, добавляемый элемент    
             */
            }
        }
        public void Remove(object value)
        {
            RemoveAt(IndexOf(value));
        }
        public void RemoveAt(int index)
        {
            if ((index >= 0) && (index < Count))
            {
                for (int i = 0; i < Count - 1; i++)
                {
                    _contents[i] = _contents[index + 1];
                }
            }
            _count--;
        }
        public void Clear()
        {
            _count = 0;
        }
        public bool Contains(object value)
        {
            bool inList = false;
            for(int i = 0; i < Count; i++)
            {
                if(_contents[i] == value)
                {
                    inList = true;
                    break;
                }
            }
            return inList;
        }
        public int IndexOf(object value)
        {
            int itemIndex = -1;
            for(int i = 0; i < Count; i++)
            {
                if(_contents[i] == value)
                {
                    itemIndex = i;
                    break;
                }
            }
            return itemIndex;
        }
        public void Insert(int index, object value)
        {
            throw new NotImplementedException();
        }
        public object this[int index]
        {
            get
            {
                return this._contents[index];
            }
            set
            {
                this._contents[index] = value;
            }
        }
        public bool IsFixedSize
        {
            get
            {
                return true;
            }
        }
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        } //элементы не редактируемые, если false
        #endregion

        #region ICollection Members
        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }
        public int Count
        {
            get
            {
                return this._count;
            }
        }
        public bool IsSynchronized //коллекция не потокобезопасная | не синхронизируемая
        {
            get
            {
                return false;
            }
        }
        #endregion

        public object SyncRoot
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// класс, генерирующий последовательность Фибоначи
    /// </summary>
    public class FibonacciSequence : IEnumerator, IEnumerable
    {
        //f1 = 1;
        //f2 = 1;
        //fi = (fi - 1) + (fi - 2);
        int[] fibonacci = new int[] { 1, 1, 2, 3, 5, 8, 13, 21 };
        int index = -1;
        #region Реализация IEnumerator
        //сеттера нет, foreach - только чтение
        public object Current
        {
            get { return fibonacci[index]; }
        }
        public bool MoveNext()
        {
            if(index == fibonacci.Length - 1)
            {
                Reset(); //нет следующего 
                //были уже звлечены все
                return false;
            }
            index++; //сдвинулись на одни
            return true; //следующий есть
            //и текущий на него настроен
        }
        public void Reset()
        {
            index = -1;
        }
        #endregion
        #region Реализация IEnumerable
        public IEnumerator GetEnumerator()
        {
            return this;
        }
        #endregion
    }
}
