    use master
      IF EXISTS(SELECT 'True' FROM sys.databases WHERE name LIKE 'db28pr8_exam_test') DROP DATABASE  db28pr8_exam_test
	  CREATE DATABASE db28pr8_exam_test
	  GO
	use [db28pr8_exam_test]
	
		CREATE TABLE [dbo].[MY_LOG](
	[MY_LOGS_ID] [int] IDENTITY(1,1) NOT NULL,
	[ACTION] [nvarchar](3) NOT NULL,
	[TBL_NAME] [nvarchar](50) NOT NULL,
	[ID_ROW] [int] NOT NULL,
	[LOG_INFO] [nvarchar](50) NOT NULL,
	[LOG_TIME] [DATE] NOT NULL,
	[COLUMN_NAME] [nvarchar](20) NOT NULL,
	[OLD_VALUE] [nvarchar](50) NOT NULL,
	[NEW_VALUE] [nvarchar](50) NOT NULL)
	ALTER TABLE [DBO].[MY_LOG] ADD CONSTRAINT PK_MY_LOGS_ID PRIMARY KEY(MY_LOGS_ID);

	INSERT INTO [dbo].[MY_LOG] ( ACTION,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME,COLUMN_NAME,
	OLD_VALUE,NEW_VALUE) VALUES ('I','[DBO].CUSTOMERS', 3,'INSERT', '2017-06-30', 'NICK_NAME','SEMEN','ANTIP' );
	select *
	from [dbo].[MY_LOG]