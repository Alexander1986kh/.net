﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamTest
{
    class Customers : IEnumerator, IEnumerable//для реализации foreach
    {
        private Customer[] customers;
        int index = -1;
        public int Length { get { return customers.Length; } }
        public Customers()
        {
            customers = new Customer[]
            {
                new Customer {Nick_name="Sidorov", Phone="102",
                    Email ="sidorov@gmail.com" ,Id_man=1},
                new Customer {Nick_name="Petrov", Phone="103",
                    Email ="petrov@gmail.com", Id_man=2 },
                new Customer {Nick_name="Semenkov", Phone="104",
                    Email ="semenkov@gmail.com", Id_man=3},
            };
        }

        internal void print(Func<Customer, bool> p)//Метод печати элементов коллекции по 
            //условию, которое передается как аргумент; p-указатель на функцию,
            //которая принимает Customer, возвращает bool
        {foreach(var i in customers)
            {
                if (p(i))
                    Console.WriteLine(i);
            }
            
        }
       

        public object Current
        {
            get
            {
                return customers[index];
            }
        }

        public IEnumerator GetEnumerator()
        {
            return this;
        }

        public bool MoveNext()
        {
            if(index==customers.Length-1)
            {
                Reset();
                return false;
            }
            else
            {
                index++;
                return true;
            }
        }

        public void Reset()
        {
            index = index - 1;
        }

    }
}
