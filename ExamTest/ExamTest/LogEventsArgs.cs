﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamTest
{
    class LogEventsArgs
    {
        private string message;
        private MyLog mylog;
        #region Properties
        public string Message
        {
            get
            {
                return message;
            }

            set
            {
                message = value;
            }
        }

        internal MyLog Mylog
        {
            get
            {
                return mylog;
            }

            set
            {
                mylog = value;
            }
        
        }
        #endregion
        private LogEventsArgs() { }
        public LogEventsArgs(string amessage, MyLog item)
        {
            message = amessage;//Строка была без инициализации, null
            mylog = item;// так же
        }
    }
}
