﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamTest
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Test Customers
            //Customers customers = new Customers();
            //foreach (var i in customers)
            //{
            //    Console.WriteLine(i);
            //}
            //customers.print(c => c.Nick_name == "Sidorov");
            //customers.print(c => c.Nick_name == "Sidorov");//Лямбда-выражение
            ////с-до стрелочки это список аргументов, с которыми работает эта функция
            ////=>- это неперегружаемый оператор(элемент синтаксиса лямбды выр-я)
            //// после стрелки тело функции
            // Func<Customer, bool> function_pointer = new Func<Customer, bool>(compare_nick_names);
            ////Создаем указатель function_pointer на функцию compare_nick_name
            // customers.print(function_pointer);
            #endregion
            Logs logs = new Logs();
            logs.LogAdded += CatchEvent;//вешаем обработчик на событие, 
            //CatchEvent лоовит событие LogAdded после генерации
            logs.FullLogAdded += CatchEvent;
            logs.Add(new MyLog(1, "New added", "Clients", 2, "was added new Client",
            DateTime.Parse("2017-07-13"), " ", " ", " "));
            
        }
       public static void CatchEvent(string message)
        {
            Console.WriteLine(message);
        }
        public static void CatchEvent(object sender,LogEventsArgs info)
        {
            Logs logs = sender as Logs;
            if (logs != null)
                Console.WriteLine("Возникло событие с {0}, Cообщение:{1},Создан субъект:{2}",
                    logs,info.Message,info.Mylog.ToString());
            else throw new ArgumentException("Sender не является объектом Logs") ;
        }
        public static bool compare_nick_names(Customer my_customer_for_search)
        {
            return (my_customer_for_search.Nick_name == "Semenkov");
        }
    }
}
