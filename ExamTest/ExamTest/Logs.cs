﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamTest
{
    class Logs : IList<MyLog>, IEnumerable
    {
        List<MyLog> mylogs_list;// написать конструктор по умолчанию
                                // Индексатор получение индекса конкретного элемента, 
                                // установить значение соответствующего [i] элемента списка
                                // в данной задаче setter не нужен
         public Logs()
        {
            mylogs_list = new List<MyLog>();
        }
        public MyLog this[int index]
        {
            get
            {
                return mylogs_list[index];
            }

            set
            {
                throw new NotImplementedException();//Logs только для чтения
            }
        }

        public int Count//Вызвать у своего контейнера метод Count, он есть у List
        {
            get
            {
                return mylogs_list.Count;
            }
        }

        public bool IsReadOnly//Проверка - только ли для чтения контейнер
        {
            get
            {
                return true;
            }
        }

        public void Add(MyLog item)
        {

            if (((object)item) != null)//Сначала приводим объект к ObjectУ, затм сравниваем,
                //т.к в реализации != еще есть == оператор
            {
                mylogs_list.Add(item);
                FullLogAdded(this, new LogEventsArgs("Added new log full info", item));
                //Sender-отправитель события(Контейнер), наследник System.Object
                LogAdded("Added new Log");
            }
        }

        public void Clear()//очищаем контейнер или count=0 или вызвать Clear у List
        {
            mylogs_list.Clear();
        }

        public bool Contains(MyLog item)//В цикле перебрать все элементы и сравнить с item
            //или вызвать у List.Containts
        {
            foreach(MyLog i in mylogs_list)
            {
                if (i.Equals(item))
                    return true;
            }
            return false;
            //mylogs_list.Contains(item);
        }

        public void CopyTo(MyLog[] array, int arrayIndex)//Вставить элементы начиная с  индекса
        {
            //mylogs_list.CopyTo(array, arrayIndex);
            if ((array.Length - arrayIndex) >= mylogs_list.Count)
                for (int i = 0, aIndex = arrayIndex; i < mylogs_list.Count; i++, aIndex++)
                {
                    array[aIndex] = mylogs_list[i];
                }
            else throw new IndexOutOfRangeException
                    ("Размер массива не позволяет проиизвести копирование");
        }

        public IEnumerator<MyLog> GetEnumerator()// возвращаем теккущий элемент массива
        {
            return mylogs_list.GetEnumerator();
        }

        public int IndexOf(MyLog item)// возвращает индекс item, если нет, то -1
        {
            int j = 0;
            foreach (MyLog i in mylogs_list)
            {

                if (i == item)
                {

                    return j;
                }
                j++;
            }
            return -1;
        }

        public void Insert(int index, MyLog item)
        {
            mylogs_list.Insert(index, item);
        }

        public bool Remove(MyLog item)
        {
             return mylogs_list.Remove(item);
        }

        public void RemoveAt(int index)
        {
            mylogs_list.RemoveAt(index); ;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
             return mylogs_list.GetEnumerator();
        }
        public delegate void MyLogHandler(string message);
        public event MyLogHandler LogAdded;//аналог события Click


        public delegate void MyLogHandlerExtenced(object sender, LogEventsArgs info);
        public event MyLogHandlerExtenced FullLogAdded;

    }
}
