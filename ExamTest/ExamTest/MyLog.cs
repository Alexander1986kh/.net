﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamTest
{
    class MyLog : IEquatable<MyLog>, ICloneable, IComparable, IComparer<MyLog>
    {
        private int my_logs_id;
        private string action;//описание действия
        private string TBL_name;//в какой таблице это произошло
        private int id_row;// id запись из той таблицы, где было действие
        private string log_info;//комментарий к действию
        private DateTime log_time;//время действия
        private string column_name;//столбец, над  которым происходило действие
        private string old_value;//старое значение
        private string new_value;
        #region Properties
        public int My_logs_id
        {
            get
            {
                return my_logs_id;
            }

            set
            {
                my_logs_id = value;
            }
        }

        public string Action
        {
            get
            {
                return action;
            }

            set
            {
                action = value;
            }
        }

        public string TBL_name1
        {
            get
            {
                return TBL_name;
            }

            set
            {
                TBL_name = value;
            }
        }

        public int Id_row
        {
            get
            {
                return id_row;
            }

            set
            {
                id_row = value;
            }
        }

        public string Log_info
        {
            get
            {
                return log_info;
            }

            set
            {
                log_info = value;
            }
        }

        public DateTime Log_time
        {
            get
            {
                return log_time;
            }

            set
            {
                log_time = value;
            }
        }

        public string Column_name
        {
            get
            {
                return column_name;
            }

            set
            {
                column_name = value;
            }
        }

        public string Old_value
        {
            get
            {
                return old_value;
            }

            set
            {
                old_value = value;
            }
        }

        public string New_value
        {
            get
            {
                return new_value;
            }

            set
            {
                new_value = value;
            }
        }
        #endregion
        private MyLog() { }
        public MyLog(int aMy_logs_id, string aAction, string aTBL_name, int aId_row, string aLog_info,
            DateTime aLog_time, string aColumn_name, string aOld_value, string aNew_value)
        {
            My_logs_id = aMy_logs_id;
            Action = aAction;
            TBL_name1 = aTBL_name;
            Id_row = aId_row;
            Log_info = aLog_info;
            Log_time = new DateTime(aLog_time.Year, aLog_time.Month, aLog_time.Day);
            Column_name = aColumn_name;
            Old_value = aOld_value;
            New_value = aNew_value;
        }

        #region Equals
        public bool Equals(MyLog obj)
        {
            if (obj == null)
                return false;
            if (this.My_logs_id == obj.My_logs_id)
                return true;
            else return false;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            MyLog tmp = obj as MyLog;
            if (tmp == null)
                return false;
            else return Equals(tmp);
        }
        #endregion
        #region Operators overloaded

        public static bool operator ==(MyLog mylog1, MyLog mylog2)
        {
            if (mylog1 == null || mylog2 == null)
                return Object.Equals(mylog1, mylog2);
            return mylog1.Equals(mylog2);
        }
        public static bool operator !=(MyLog mylog1, MyLog mylog2)
        {
            if (mylog1 == null || mylog2 == null)
                return !Object.Equals(mylog1, mylog2);
            return !(mylog1.Equals(mylog2));
        }
        public static bool operator >(MyLog mylog1, MyLog mylog2)
        {
            return (mylog1.my_logs_id > mylog2.my_logs_id);
        }
        public static bool operator <(MyLog mylog1, MyLog mylog2)
        {
            return (mylog1.my_logs_id < mylog2.my_logs_id);
        }

        #endregion
        #region Overrided metods
        public override string ToString()// Переопределенный виртуального метода
        {
            return String.Format("My logs id: {0} Action: {1} Table name: {2} Id row: {3}" +
                "Log info: {4} Log time: {5} Column name: {6} Old value: {7} New value: {8}",
                My_logs_id, Action, TBL_name1, Id_row, Log_info, Log_time, Column_name,
                Old_value, New_value);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
        #region IClonable
        public object Clone()
        {

            object clone = this.MemberwiseClone();
            MyLog my_clone_log = clone as MyLog;
            if (my_clone_log == null)
                throw new ArgumentException("Обьект не является экземпляром класса MyLog");
            else
            {
                my_clone_log.action = string.Copy(action);
                my_clone_log.TBL_name = string.Copy(TBL_name);
                my_clone_log.log_info = string.Copy(log_info);
                my_clone_log.log_time = new DateTime(log_time.Year, log_time.Month, log_time.Day);
                my_clone_log.column_name = string.Copy(column_name);
                my_clone_log.old_value = string.Copy(old_value);
                my_clone_log.new_value = string.Copy(new_value);
            }
            return my_clone_log;
        }
        #endregion
        #region Compare
        public int CompareTo(object obj)//возвращает 0- если равны, -1-если obj>, 1, если obj<
        {
            if (obj == null)
                return 1;
            MyLog newlog = obj as MyLog;// объект скопируется, или null, если не получилось
            if (newlog == null)
                throw new ArgumentException("Обьект не является экземпляром класса MyLog");
            else
                return this.my_logs_id.CompareTo(newlog.my_logs_id);
        }

        public int Compare(MyLog x, MyLog y)// возвращает 0, -1, 1, сравнивает 2 обьекта-аргумента
        {
            return x.CompareTo(y);
        }
        #endregion
    }
}
