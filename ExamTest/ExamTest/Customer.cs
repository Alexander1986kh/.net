﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamTest
{
    public class Customer
    {
        public string Nick_name { set; get; }
        public string Phone { set; get; }
        public string Email { set; get; }
        public int Id_man { set; get; }
        public Customer()
        {
            //Nick_name = String.Empty;
            //Phone = String.Empty;
            //Email = String.Empty;
            //Id_man = 0;
        }
        public Customer(string aNick_name, string aPhone, string aEmail, int aId_man)
        {
            Nick_name = aNick_name;
            Phone = aPhone;
            Email = aEmail;
            Id_man = aId_man;
        }
        public override string ToString()
        {
            return String.Format("Nick name: {0} Phone: {1} Email: {2} Id man: {3}", Nick_name,
                Phone, Email, Id_man);
        }
    }
}