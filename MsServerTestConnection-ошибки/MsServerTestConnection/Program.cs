﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;//для работы с файлом App.config из  System.Configuration.dll
using System.Data.SqlClient;//из драйвера ms sql сервера
using System.Data.SqlTypes;//system data .dll
namespace MsServerTestConnection
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = @"Data Source=PC36-8-Z;Initial Catalog=db28pr8;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                StringBuilder sqltext = new StringBuilder();
                sqltext.Append("SELECT");
                sqltext.Append("[dbo].[CUSTOMERS].EMAIL AS MY_EMAIL,");
                sqltext.Append(" [dbo].[CUSTOMERS].NIK_NAME, ");
                sqltext.Append("[dbo].[CUSTOMERS].PHONE,");
                sqltext.Append("[dbo].[CUSTOMERS].ID_MAN, ");
                sqltext.Append("[dbo].[MEN].LAST_NAME,");
                sqltext.Append("[dbo].[MEN].NAME,");
                sqltext.Append(" [dbo].[MEN].YEAR");
                sqltext.Append("FROM");
                sqltext.Append("[dbo].[CUSTOMERS]");

                sqltext.Append("LEFT JOIN[dbo].[MEN]");

                sqltext.Append("ON[dbo].[MEN].ID=[dbo].[CUSTOMERS].ID_MAN");
	
                using (SqlCommand command = new SqlCommand("SELECT * FROM [db28pr8].[dbo].[CUSTOMERS]", con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int v1 = reader.GetInt32(0);
                        string v2 = reader.GetString(1);//=reader
                        string v3 = reader.GetString(2);
                        string v4 = reader.GetString(3);

                        //DateTime v4 = reader.GetDateTime(4);
                        Console.WriteLine("{0} {1} {2} {3}", v1,v2,v3,v4);
                        Customers person = new Customers(reader.GetString(1), reader.GetString(2), reader.GetString(3),
                            reader.GetInt32(4), reader.GetString(5), reader.GetString(6), reader.GetInt32(7));
                    }
                }
            }
        }
        public class Customers
        {
            private string nick_name;
            private string phone;
            private string email;
            private int id_men;
            private string lname;
            private string sname;
            private int year;

            protected string Nick_name
            {
                get
                {
                    return nick_name;
                }

                set
                {
                    nick_name = value;
                }
            }

            protected string Phone
            {
                get
                {
                    return phone;
                }

                set
                {
                    phone = value;
                }
            }

            protected string Email
            {
                get
                {
                    return email;
                }

                set
                {
                    email = value;
                }
            }

            protected int Id_men
            {
                get
                {
                    return id_men;
                }

                set
                {
                    id_men = value;
                }
            }

            protected string Lname
            {
                get
                {
                    return lname;
                }

                set
                {
                    lname = value;
                }
            }

            protected string Sname
            {
                get
                {
                    return sname;
                }

                set
                {
                    sname = value;
                }
            }

            protected int Year
            {
                get
                {
                    return year;
                }

                set
                {
                    year = value;
                }
            }
            public Customers(string anick_name, string aphone, string aemail,
                int aid_men, string alname, string asname, int ayear)
            {
                nick_name = anick_name;
                phone = aphone;
                email = aemail;
                id_men = aid_men;
                lname = alname;
                sname = asname;
                year = ayear;
            }
            #region Entity part
            //тексты запросов
            //select all
            //select One for Id
            //insert
            //update
            //delete
            #endregion
            public override string ToString()
            {
                return base.ToString();
            }
            
        }
    }
}
