﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OpenSaveFile
{
    public partial class FileControl : UserControl
    {
        public FileControl()
        {
            InitializeComponent();
        }
        #region Обработка Click-ов
        //"моё" событие - click по кнопке (имя BtnClick - придумано самостоятельно)
        public event System.EventHandler BtnClick;
        //обработчик для события: имя подчиняется чёткому правилу
        //OnИмяСобытие - регистрочувствительно с приставкой On
        //нужен для программного присваивания обработчика только моей кнопке
        public void OnBtnClick(Object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Open File";
            dlg.Filter = "All files (*.*)|*.*";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                txtFileControl.Text = dlg.FileName;
            }
            dlg.Dispose();
        }
        //Обработчие общего клика по всему составному компоненту
        //нужен для программного присваивания обработчика Click
        public void OnClick(Object sender, EventArgs e)
        {
            if (BtnClick != null)
            {
                BtnClick(sender, e);
            }
            else
            {
                btnFileControl.Text = "Спасибо за Ваш клик!";
            }
        }
        //необходимо для обработки клика от пользователя
        private void btnFileControl_Click(object sender, EventArgs e)
        {
            if (e == null)
            {
                e = new EventArgs();
            }
            if (BtnClick != null)
                this.OnBtnClick(this, e);
        }
        #endregion
        #region "Глобальное" свойство Text
        [Browsable (true)]
        public override string Text
        {
            get
            {
                return txtFileControl.Text;
            }

            set
            {
                txtFileControl.Text = value;
            }
        }
        #endregion
        #region отрисовка
        private void FileControl_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            gr.FillEllipse(Brushes.Red, btnFileControl.Left, btnFileControl.Width, btnFileControl.Top, 20);
        }
        #endregion
    }
}
