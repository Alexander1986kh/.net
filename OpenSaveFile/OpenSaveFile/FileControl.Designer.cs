﻿namespace OpenSaveFile
{
    partial class FileControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFileControl = new System.Windows.Forms.Button();
            this.txtFileControl = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnFileControl
            // 
            this.btnFileControl.Location = new System.Drawing.Point(405, 8);
            this.btnFileControl.Margin = new System.Windows.Forms.Padding(4);
            this.btnFileControl.Name = "btnFileControl";
            this.btnFileControl.Size = new System.Drawing.Size(112, 34);
            this.btnFileControl.TabIndex = 1;
            this.btnFileControl.TabStop = false;
            this.btnFileControl.Text = "...";
            this.btnFileControl.UseVisualStyleBackColor = true;
            this.btnFileControl.Click += new System.EventHandler(this.btnFileControl_Click);
            // 
            // txtFileControl
            // 
            this.txtFileControl.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtFileControl.Location = new System.Drawing.Point(16, 13);
            this.txtFileControl.Margin = new System.Windows.Forms.Padding(4);
            this.txtFileControl.Name = "txtFileControl";
            this.txtFileControl.Size = new System.Drawing.Size(369, 26);
            this.txtFileControl.TabIndex = 0;
            // 
            // FileControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtFileControl);
            this.Controls.Add(this.btnFileControl);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FileControl";
            this.Size = new System.Drawing.Size(548, 60);
            this.Click += new System.EventHandler(this.OnClick);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FileControl_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFileControl;
        private System.Windows.Forms.TextBox txtFileControl;
    }
}
