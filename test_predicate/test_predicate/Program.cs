﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using point.lib;

namespace test_predicate

{
    class Program
    {
//        1) Написать функцию public static void DoSomething
//для которой будут реализованы следующие перегрузки:
//public static void DoSomething(int[] arr, Predicate<int> p),
//public static void DoSomething(int[] arr, Func<int, double> p),
//public static void DoSomething(int[] arr, Action<int, string> p)

// DoSomething внутри себя должна вызывать переданную в нее по указателю функцию
// для каждого из эл-тов массива и выводить в консоль результат, который вернет
//вызванная ф-ция.

//Функции, которые должны быть переданы в DoSomething по указателю(через Predicate,
//Func или Action):
//public static bool isOdd(int a) - проверка на четность
//+ написать аналогичное лямбда-выражение
//public static double calcSquare(int radius) - расчет площади круга(если эл-ты массива - это
//величины радиусов)
//+ написать аналогичное лямбда-выражение
//public static string print(int a) - возвращает строку с текстом: "Текущее значение: /значение а/"
//+ написать аналогичное лямбда-выражение




        static void Main(string[] args)
        {
            #region Last
            //Point p1 = new Point(234, 543);
            //Point p2 = new Point(-234, 543);
            //my_delegate deleg = new my_delegate(print);
            //operation(234, 4.87654, "kjhgfds", deleg);
            //Action<int, double, string> my_action;
            //my_action = print;
            //operation(2, 3.2345, "uytfdressertyui", my_action);
            //operation(2345, 3.2345, "uytfdressertyui", my_action);
            //Predicate<Point> pred;// = delegate (Point apoint) { return apoint.X > apoint.Y; };
            //pred = isPositive;
            //print_if(p2, pred);
            //Func<Point, bool> functor;
            //functor = isPositive;
            ////print_if(p1, functor);
            ////print_if(p2, functor);
            //print_if(p1, p => p.X < p.Y);//Лямбда выражения
            #endregion
            #region first part
            Random rand = new Random();
            int[] mas = new int[10];
            for (int i = 0; i < mas.Length; i++)
                mas[i] = rand.Next(-20, 20);
                Predicate<int> pred = delegate (int x) { return x < 0; };
            DoSomething(mas, pred);
            Func<int,int , double> myFunc = delegate (int sum_el,int size_mas) {
                return (double)sum_el / size_mas;
            };
            DoSomething(mas, myFunc);
            for (int i = 0; i < mas.Length; i++)
                Console.WriteLine(isOdd(mas[i]));
            Action<int> myaction = delegate (int x) { Console.WriteLine
                (String.Format("Значение: {0}", x)); };
            DoSomething(mas, myaction);
            Action<int> myaction1 = new Action<int>(myfunction);
            DoSomething(mas, myaction1);

            Predicate<int> is_number_odd = x => (x % 2 == 0);
            Predicate<int> is_number_odd1 = new Predicate<int>(isOdd);
            Console.WriteLine(is_number_odd(mas[2]));
            Func<int, double> square_circle = new Func<int, double>(calcSquare);
            DoSomething(mas, square_circle);
            Func<int, double> square_circle1 = x=> Math.PI*Math.Pow(x,2);
            DoSomething(mas, square_circle1);
            Console.WriteLine("222222222222222222222222222222222222222222");
            #endregion
            #region second part
            Func<int, int, int, int> triangle_square_functor = 
                delegate (int a, int b, int c) { return a + b + c; };
            int A = 2;
            int B = 5;
            int C = 6;
            calcTriangle(A, B, C, triangle_square_functor);
            Func<int, int, int, string> about_triangle =
                delegate (int a, int b, int c) {
                    if (a == b || b == c || a == c)
                        return ("Треугольник равнобедренный");
                    else return "Треугольник общего типа";
                };
            aboutTriangle(A, B, C, about_triangle);
            Func<int, int, int, bool> is_triangle =
               delegate (int a, int b, int c) { return (a +b>c)&&(a+c>b)&&(b+c>a); };
            isTriangle(A, B, C, is_triangle);
            #endregion
        }
        public static void print(int a, double d, string str)
        {
            Console.WriteLine(String.Format("a: {0} d:{1} str: {2}", a, d, str));
        }
        //public static void print_if(Point p, Func<Point, bool> func)
        //{
        //    if (func(p))
        //        Console.WriteLine(p);
        //    else Console.WriteLine("Условие не выполнено");
        //}
        public static void operation(int a, double d, string str, Action<int, double, string> action)
        {
            if (a > 34)
                action(a, d, str);
            else Console.Write(0);
        }
        public static void operation(int a, double d, string str, my_delegate del)
        {
            if (a > 34)
                del(a, d, str);
            else Console.Write(0);
        }
        public delegate void my_delegate(int a, double d, string str) ;
        public static bool isPositive(Point apoint)
        {
            return (apoint.X > 0 && apoint.Y > 0) ? true : false;
        }
        public static void print_if(Point p, Predicate<Point> apredicate) {
            if (apredicate(p) == true)
                Console.WriteLine(p);
            else Console.WriteLine("Условие не выполнено");
        }
        public static void DoSomething(int[] arr, Predicate<int> p)
        {
            for(int i=0; i < arr.Length; i++)
            {
                if (p(arr[i]) == true)
                    Console.WriteLine(arr[i]);
            }
        }
        public static double DoSomething(int[] arr, Func<int,int, double> p)
        {
            int sum=0;
            for (int i = 0; i < arr.Length; i++)
            {
                sum = sum+arr[i];
            }
            return p(sum,arr.Length); 

        }

        public static void DoSomething(int[] arr, Action<int> p){
            for(int i = 0; i < arr.Length; i++)
            {
                p(arr[i]);
            }
        }
        public static void myfunction(int ax)
        {
            {
                Console.WriteLine
                  (String.Format("Значение: {0}", ax));
            }
        }
       public static bool isOdd(int value)
        {
            return value % 2 == 0;
        }
        public static double calcSquare(int radius)
        {
            return (double)Math.PI * Math.Pow(radius, 2);
        }

        public static void DoSomething(int[] arr, Func<int,double> p)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(p(arr[i]));
            }
        }
        #region second part
        //2)Написать ф-цию public static void calcTriangle(int a, int b, int c, Func<...> p) //возможны перегрузки в зависимости от используемого делегата
        //которая принимает длины сторон треугольника
        //Написать и передать в calcTriangle с помощью делегата функцию, которая:
        //-считает и возвращает периметр треугольника
        //-сообщает, существует ли такой треугольник
        //-возвращает строку "равнобедренный" или "не равнобедренный"
        //calcTriangle должна распечатать результат вызова делегата.
        public static void calcTriangle(int a, int b, int c, Func<int, int, int, int> p)
        {
            Console.WriteLine(p (a, b, c));
        }
        public static string aboutTriangle(int a, int b, int c, Func<int, int, int, string> p)
        {
          return p(a, b, c);
        }
        public static void isTriangle(int a, int b, int c, Func<int, int, int, bool> p)
        {
            if (p(a, b, c))
                Console.WriteLine("Треугольник существует");
            else
                Console.WriteLine("такого треугольника нету");
        }
        #endregion
       
    }

}
