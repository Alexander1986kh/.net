﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace MSServerTestConnetion
{
    class Program
    {
        static void Main(string[] args)
        {
            //string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            string connectionString = @"Data Source=PC36-8-P;Initial Catalog=db28pr8;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("SELECT TOP 1000 [CLIENT_ID],[CLIENT_NAME] FROM [db28pr8].[CLIENTS]", con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        //int v1 = reader.GetInt32(0);
                        string v1 = reader.GetString(0);
                        string v2 = reader.GetString(1);
                        DateTime v3 = reader.GetDateTime(2);
                        Console.WriteLine("{0} {1} {2}", v1, v2, v3);
                    }
                }
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmdSelect = new SqlCommand();
                    SqlParameter currencyCodeParam = new SqlParameter("@pCultureID", System.Data.SqlDbType.NVarChar);
                    string cultureIdCode = "ar";
                    currencyCodeParam.Value = cultureIdCode;
                    cmdSelect.Parameters.Add(currencyCodeParam);
                    cmdSelect.CommandText = "SELECT TOP 1000 [CLIENT_ID],[CLIENT_NAME] FROM [db28pr8].[CLIENTS] where CLIENT_ID=@pCultureID";
                    cmdSelect.Connection = conn;
                    conn.Open();
                    SqlDataReader reader = cmdSelect.ExecuteReader();
                    while (reader.Read())
                    {
                        //int v1 = reader.GetInt32(0);
                        string v1 = reader.GetString(0);
                        string v2 = reader.GetString(1);
                        DateTime v3 = reader.GetDateTime(2);
                        Console.WriteLine("{0} {1} {2}", v1, v2, v3);
                    }
                    reader.Close();
                    conn.Close();
                }
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand cmdStoredProc = new SqlCommand();
                    SqlParameter inParam1 = new SqlParameter("@p1", System.Data.SqlDbType.Int);
                    SqlParameter inParam2 = new SqlParameter("@p2", System.Data.SqlDbType.Int);
                    inParam1.Value = "1";
                    inParam2.Value = "2";

                    SqlParameter outvalOUT_PARAM1 = new SqlParameter("@out_sum", System.Data.SqlDbType.VarChar, 200);
                    outvalOUT_PARAM1.Direction = System.Data.ParameterDirection.Output;
                    cmdStoredProc.Parameters.Add(outvalOUT_PARAM1);

                    cmdStoredProc.Parameters.Add(inParam1);
                    cmdStoredProc.Parameters.Add(inParam2);
                    cmdStoredProc.CommandText = "PROC_FOR_TEST";
                    cmdStoredProc.CommandType = System.Data.CommandType.StoredProcedure;

                    cmdStoredProc.Connection = conn;
                    conn.Open();
                    cmdStoredProc.ExecuteNonQuery();
                    Console.WriteLine("out value is {0}", outvalOUT_PARAM1.Value);
                    conn.Close();
                }
                /*
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlCommand InsertCurrencyCommand = new SqlCommand();
                    SqlParameter currencyCodeParam = new SqlParameter("@CurrencyCode",System.Data.SqlDbType.NVarChar);
                    SqlParameter nameParam = new SqlParameter("@Name", System.Data.SqlDbType.NVarChar);
                    currencyCodeParam.Value = currencyCode;
                    nameParam.Value = name;
                    InsertCurrencyCommand.Parameters.Add(currencyCodeParam);
                    InsertCurrencyCommand.Parameters.Add(nameParam);
                    InsertCurrencyCommand.CommandText =
                        "INSERT Sales.Currency (CurrencyCode, Name, ModifiedDate)" +
                        " VALUES(@CurrencyCode, @Name, GetDate())";
                    InsertCurrencyCommand.Connection = conn;
                    conn.Open();
                    InsertCurrencyCommand.ExecuteNonQuery();
                    conn.Close();
                }
                */
            }
        }
    }
}
