﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CodePageStream
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime timeAction = DateTime.Now;
            StreamWriter stream = new StreamWriter("D:\\MyData\\test.txt", true, System.Text.Encoding.UTF8);

            stream.Write(timeAction.Day.ToString() + ".");
            stream.Write(timeAction.Month.ToString() + ".");
            stream.Write(timeAction.Year.ToString() + " ");
            stream.Write(timeAction.Hour.ToString() + ":");
            stream.Write(timeAction.Minute.ToString() + ":");
            stream.Write(timeAction.Second.ToString() + " ");

            stream.WriteLine("Запись в файл состоялась");
            stream.Close();//файл физически создан на диске
            string readPath = @"D:\MyData\test.txt";// Символ @ убирает необходимость экранировать
            string writePath = @"D:\MyData\newtest.txt";
            string text = String.Empty;
            try
            {
                using(StreamReader sr =new StreamReader(readPath, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                }
                using (StreamWriter sw = new StreamWriter(writePath,false, System.Text.Encoding.Default))//запись с нуля, все чистит и пишет
                {
                    sw.WriteLine(text);
                }
                using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))//Дозапись
                {
                    sw.WriteLine("Дозапись");
                    sw.Write(4.5);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            text = "hello world";
            //запись в файл
            using (FileStream fstream = new FileStream(@"D:\MyData\note.dat", FileMode.OpenOrCreate))
            {
                //Преобразуем строку в байты
                //ASCIIEncoding encoding = new ASCIIEncoding();
                //UTF8Encoding encoding = new UTF8Encoding();
                //Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                byte[] input = Encoding.Default.GetBytes(text);// Используется кодировка операционной системы
                //запись массива байтов в файл
                fstream.Write(input, 0, input.Length);
                Console.WriteLine("Текст записан в файл");
                //перемещаем курсор в конец файла, до конца файла 5 байт
                fstream.Seek(-5, SeekOrigin.End);// Минус 5 символов с конца потока
                //считываем четыре символа с текущей позиции
                byte[] output = new byte[4];
                fstream.Read(output, 0, output.Length);//<=>fstream.Read(output,0,4)
                //Декодируем байты в строку
                string textFromFile = Encoding.Default.GetString(output);
                Console.WriteLine("Текст из файла : {0}", textFromFile);//worl
                //заменяем слово world на слово house
                string replaceText = "house";
                fstream.Seek(-5, SeekOrigin.End);// минус 5 символов с конца потока
                input = Encoding.Default.GetBytes(replaceText);//Преобразовал содержимое replaceText в байты
                fstream.Write(input, 0, input.Length);//Записал в поток fstream(тот, где был сдвиг)
                //считываем весь файл
                //возвращаем указатель в начало файла
                fstream.Seek(0, SeekOrigin.Begin);
                output = new byte[fstream.Length];
                //декодируем байты в строку
                textFromFile = Encoding.Default.GetString(output);
                Console.WriteLine("Текст из файла: {0}", textFromFile);

            }
        }
    }
}
