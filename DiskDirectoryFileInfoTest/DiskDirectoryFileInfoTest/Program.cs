﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using System.Xml;

namespace DiskDirectoryFileInfoTest
{
    class Program
    {
        public static void test_message(string message)
        {
            Console.WriteLine(message);
        }
        static void Main(string[] args)
        {

            string connctionString = System.Configuration.ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;

            Console.WriteLine(connctionString);
            string provaiderName = ConfigurationManager.ConnectionStrings[4].ProviderName;
            //Console.WriteLine("connectionString= {0}\nprovaiderName ={1}", connctionString, provaiderName);
            using (XmlWriter writer = XmlWriter.Create("d:\\mydata1\\myappconfig.xml"))
            {
                writer.WriteStartDocument();
                writer.WriteRaw("\n");
                writer.WriteStartElement("TEST");
                writer.WriteAttributeString("my_atr1", "значение 1");
                writer.WriteAttributeString("my_atr2", "значение 1");
                writer.WriteAttributeString("my_atr3", "значение 1");
                writer.WriteRaw("\n");
                writer.WriteStartElement("TEST_HEAD");
                writer.WriteAttributeString("my_atr1", "значение 1");
                writer.WriteAttributeString("my_atr2", "значение 1");
                writer.WriteAttributeString("my_atr3", "значение 1");
                writer.WriteElementString("elem1", "значение");
                writer.WriteRaw("\n");
                writer.WriteEndDocument();
            }
            using (XmlWriter writer = XmlWriter.Create("d:\\mydata1\\configmy.xml"))
            {
                writer.WriteStartDocument();
                writer.WriteRaw("\n");
                writer.WriteStartElement("TEST");
                writer.WriteAttributeString("my_atr1", "значение 1");
                writer.WriteAttributeString("my_atr2", "значение 1");
                writer.WriteAttributeString("my_atr3", "значение 1");
                writer.WriteRaw("\n");
                writer.WriteStartElement("TEST_HEAD");
                writer.WriteAttributeString("my_atr1", "значение 1");
                writer.WriteAttributeString("my_atr2", "значение 1");
                writer.WriteAttributeString("my_atr3", "значение 1");
                writer.WriteElementString("elem1", "значение");
                writer.WriteRaw("\n");
                writer.WriteEndDocument();
            }
            using (XmlReader reader = XmlReader.Create("d:\\mydata1\\myconfig.xml"))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        if (reader.IsEmptyElement)
                            Console.WriteLine("<{0}/>", reader.Name);
                       
                        else
                        {
                            Console.Write("<{0}> ", reader.Name);
                            reader.Read(); // Read the start tag.
                            if (reader.IsStartElement())  // Handle nested elements.
                            {   if (reader.Name == "database")
                                {
                                    string host = reader.GetAttribute("host");
                                    string user = reader.GetAttribute("user");
                                    string password = reader.GetAttribute("password");
                                }
                                    Console.Write("\r\n<{0}>", reader.Name);
                                    Console.WriteLine(reader.ReadString());  //Read the text content of the element.
                                }
                                }
                    }
                }
            }
            Console.WriteLine("press any key...");
            Console.ReadKey();
            NewMethod();
            /*
            MyClass men = new MyClass();
            men.Add += test_message;
            string test = String.Empty,
            test1 = String.Empty;
            Console.Write("Введите первое слово: ");
            test = Console.ReadLine();
            Console.Write("Введите второе слово: ");
            test1 = Console.ReadLine();
            men.SetMen(test, x => x == test1);
            */
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        private static void NewMethod()
        {
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (DriveInfo drive in drives)
            {
                Console.WriteLine("Название: {0}", drive.Name);
                Console.WriteLine("Тип: {0}", drive.DriveType);
                Console.WriteLine("Формат файловой системы: {0}", drive.DriveFormat);
                if (drive.IsReady)
                {
                    Console.WriteLine("Объём диска: {0}", drive.TotalSize / 1048576 + " Mb");
                    Console.WriteLine("Свободное пространство: {0}", drive.TotalFreeSpace / 1048576 + " Mb");
                    Console.WriteLine("Доступное свободное пространство: {0}", drive.AvailableFreeSpace / 1048576 + " Mb");
                    Console.WriteLine("Метка: {0}", drive.VolumeLabel);
                }
                Console.WriteLine();
            }
        }
        public static string[] GetFiles(string path)
        {
            ArrayList res = new ArrayList();
            foreach(string file in Directory.GetFiles(path))
            {
                res.Add(file);
            }
            foreach (string dir in Directory.GetDirectories(path))
                res.AddRange(GetFiles(dir));
            return (string[])res.ToArray(typeof(string));
        }
        //public static sum_elements_array(int[] array, int index,out int  sum)
        //{

        //    if (index == 0)
        //        sum += array[index];
        //    else sum_elements_array(array, index--, sum);

        //}
    }
    /*public class MyClass
    {
        private string men;
        public delegate void EventHandler(string message);
        public event EventHandler Add;
        public string MEN
        {
            get { return this.men; }
            set { this.men = value; }
        }
        public MyClass() { this.men = String.Empty; }
        public void SetMen(string set, Func<string, bool> op)
        {
            if (op(set))
            {
                this.men = set;
                if (Add != null)
                {
                    Add("Right choise - " + this.men + "\nYeees! The words is equals");
                }
            }
            else
            {
                if (Add != null)
                {
                    Add("Wrong choise!! The words is not equals");
                }
            }
        }
    }*/
}
