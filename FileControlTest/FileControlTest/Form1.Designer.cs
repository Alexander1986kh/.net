﻿namespace FileControlTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileControl1 = new OpenSaveFile.FileControl();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fileControl1
            // 
            this.fileControl1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fileControl1.Location = new System.Drawing.Point(13, 34);
            this.fileControl1.Margin = new System.Windows.Forms.Padding(4);
            this.fileControl1.Name = "fileControl1";
            this.fileControl1.Size = new System.Drawing.Size(915, 69);
            this.fileControl1.TabIndex = 0;
            this.fileControl1.BtnClick += new System.EventHandler(this.fileControl1_Click);
            this.fileControl1.Click += new System.EventHandler(this.fileControl1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(199, 238);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 437);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.fileControl1);
            this.Name = "Form1";
            this.Text = "MainForm";
            this.ResumeLayout(false);

        }

        #endregion

        private OpenSaveFile.FileControl fileControl1;
        private System.Windows.Forms.Button button1;
    }
}

